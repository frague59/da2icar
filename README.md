# Demo site for DA2I students

The purpose of this site is to show some usages of the [django framework](https://www.djangoproject.com/)

## Usage

``` sh 
cd Projects/
git clone https://gitlab.com/frague59/da2icar.git
cd da2icar/
python3 -m venv ./venv
. venv/bin/activate
pip install pip -U # Upgrades pip
pip install -r ./requirements.txt
pip install -r ./requirements-dev.txt
export DJANGO_settings_MODULE=da2icar.settings.dev
./manage.py migrate # Creates the database
./manage.py createsuperuser
./manage.py runserver 
```
With your browser, go to:

http://localhost:8000/

Enjoy :°)

## What do you have to do ?

+ Updates the templates to make it usable with django
+ Create class-based views for list and detail of cars
+ Adds a model to book a car
+ Make a form view to create this booking

## If there are some admins arround, may be we can make a production install:
+ gunicorn server (through pip)
+ front apache2 server (or nginx) though an unix socket or 
  a simple 0.0.0.0:XXXX binding (for testing purpose)
+ Deploy a postgres instance (for P. Mathieu...)

## Attended prod tree

```
/var/www/da2icar.example.com/
+ src/   # Project directory (BASE_DIR)
  + da2icar/
  + rental/
  + templates/
  + assets/
+ tmp/
+ media/  # served by apache2
+ static/ # served by apache2
+ var/ 
  + logs/
  + gunicorn.socket
```

