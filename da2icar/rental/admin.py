# -+- coding: utf-8 -+-
"""
Models for the :class:`rental` application
"""
import logging
from django.contrib import admin
from rental import models 
from reversion.admin import VersionAdmin 
logger = logging.getLogger("rental.admin")

@admin.register(models.Category, models.Brand)
class LabelAdmin(VersionAdmin):
    list_display= ("label", )


@admin.register(models.CarModel)
class CarModelAdmin(VersionAdmin):
    list_display = ("label", "brand", "millesim")


@admin.register(models.Vehicle)
class VehicleAdmin(VersionAdmin):
    list_display = ("licence", "car_state", "bought_at")