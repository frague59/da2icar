# -+- coding: utf-8 -+-
"""
Models for the :class:`rental` application
"""
import logging
import reversion
from django.utils.translation import ugettext_lazy as _
from django.db import models
from enum import IntEnum

logger = logging.getLogger("rental.models")


class CarState(IntEnum):
    New = 1
    Used = 2
    Old = 3
    OutOfOrder = 10


CAR_STATE_CHOICES = [
    (CarState.New.value, _("New")),
    (CarState.Used.value, _("Used")),
    (CarState.Old.value, _("Old")),
    (CarState.OutOfOrder.value, _("Out of order")),
]


@reversion.register
class Brand(models.Model):
    """
    Brand car
    """

    label = models.CharField(verbose_name=_("name"), max_length=255)

    def __str__(self):
        return self.label

    class Meta:
        verbose_name = _("Brand")
        verbose_name_plural = _("Brands")
        ordering = ("label",)


@reversion.register
class Category(models.Model):

    label = models.CharField(verbose_name=_("lbel"), max_length=255)

    def __str__(self):
        return self.label

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")
        ordering = ("label",)


@reversion.register
class CarModel(models.Model):

    label = models.CharField(verbose_name=_("label"), max_length=50)

    brand = models.ForeignKey(
        "rental.Brand",
        verbose_name=_("brand"),
        on_delete=models.CASCADE,
        related_name="brand_cars",
    )

    millesim = models.DateField(verbose_name=_("millesin"), null=True, blank=True)

    category = models.ForeignKey(
        "rental.Category",
        verbose_name=_("category"),
        on_delete=models.CASCADE,
        related_name="category_cars",
    )

    def __str__(self):
        return "{brand} {label} ({millesin})".format(
            label=self.label, millesim=self.millesim.strftime("Y")
        )


@reversion.register
class Vehicle(models.Model):

    licence = models.CharField(_("licence number"), max_length=50)

    car_model = models.ForeignKey(
        "rental.CarModel", verbose_name=_(""), on_delete=models.CASCADE
    )

    bought_at = models.DateField(verbose_name=_("baught at"))

    car_state = models.PositiveSmallIntegerField(
        verbose_name=_("state"), choices=CAR_STATE_CHOICES, default=1
    )

    class Meta:
        verbose_name = _("Vehicle0")
        verbose_name_plural = _("Vehicles")
