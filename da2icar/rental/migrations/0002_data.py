# Generated by Django 2.2.8 on 2019-12-04 21:09

from django.db import migrations

BRANDS = [
    {"label": "Ford"}, 
    {"label": "Dodge"},
    {"label": "Tesla"},
    {"label": "Chevrolet"},
    {"label": "Pontiac"},
    {"label": "Cadillac"},
]


CATEGORIES = [
    {"label": "S.U.V"}, 
    {"label": "Sports car"}, 
    {"label": "Muscle car"}, 
    {"label": "Poney car"}, 
    {"label": "Truck"}, 
    {"label": "Family"}, 
]


def fwd_brands(apps, schema_editor):
    if schema_editor.connection.alias != 'default':
        return
    brand_model = apps.get_model("rental", "Brand")
    for brand in BRANDS:
        _brand = brand_model(**brand)
        _brand.save()


def rev_brands(apps, schema_editor):
    if schema_editor.connection.alias != 'default':
        return
    brand_model = apps.get_model("rental", "Brand")
    for brand in BRANDS:
        _brand = brand_model.objects.get(**brand)
        _brand.delete()


def fwd_categories(apps, schema_editor):
    if schema_editor.connection.alias != 'default':
        return
    category_model = apps.get_model("rental", "Category")
    for category in CATEGORIES:
        _cat = category_model(**category)
        _cat.save()


def rev_categories(apps, schema_editor):
    if schema_editor.connection.alias != 'default':
        return
    category_model = apps.get_model("rental", "Category")
    for category in CATEGORIES:
        _cat = category_model.objects.get(**category)
        _cat.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('rental', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(fwd_brands, rev_brands ,atomic=True),
        migrations.RunPython(fwd_categories, rev_categories, atomic=True),
    ]
