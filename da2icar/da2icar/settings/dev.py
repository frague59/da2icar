# -+- coding: utf-8 -+-

from pathlib import Path
from .base import *

DEBUG = True
SECRET_KEY = "NO SECRET HERE..."

##########################
# Paths
##########################
BASE_DIR = Path(__file__).parents[3]
STATIC_ROOT = BASE_DIR / "assets"

##########################
# DEBUG TOOLBAR
##########################
INSTALLED_APPS.append("debug_toolbar") 

MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')